# Database Project
This repository is for database project in [DTU Compute](https://compute.dtu.dk) course [02327-F20](https://kurser.dtu.dk/course/02327).

## Files
In general this repository is **ONLY** for the use of `.sql` files. Please create your files in a way that allows all users to run them.

## Branches
- `master`: Please us this branch for all ready code.
- `dev`: Please use this branch for all development related stuff.